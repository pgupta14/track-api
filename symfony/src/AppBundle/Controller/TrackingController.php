<?php
//src/AppBundle/Controller/TrackingController.php
namespace AppBundle\Controller;
require_once 'TrackDB/TrackObj.php';
require_once 'TrackDB/Flat.php';
//require_once 'TrackDB/Pot.php';
//require_once 'TrackDB/Plant.php';
require_once 'TrackDB/DbConnect.php';
require_once 'TrackDB/UsefulFunctions.php';
require_once 'TrackDB/Construct.php';

ini_set("log_errors", 1);
ini_set("error_log", "php-error.log");
//error_log( "Hello, errors----------------!" );

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;



class TrackingController extends Controller
{
    /**
    *@Route("/api/tracking/{crop}/upload")
    *@Method("POST")
    */
    public function uploadAction(Request $request,$crop)
    {
        $RowInFile = 2;
        $request_body = $request->getContent();
        $data_decode = json_decode($request_body,true);
        $dbConnect = new TrackDB\DbConnect();
        $conn = $dbConnect->connect();
        
        $resFlatId = TrackDB\checkForFlatId($data_decode[0]['Flat_id'],$dbConnect);
        if (!$resFlatId){return new Response ("Flat id doesn't exits in DB");};
        $TypeIdRes = TrackDB\getTypeId($resFlatId,$dbConnect);
        $totalPots = TrackDB\checkForTotalChild($TypeIdRes);
        //return new Response($totalPots);
        
        //Validation 1: if more pots information exits in file than required
        if (count($data_decode) <= $totalPots){
            $rowInserted=1;
            foreach($data_decode as $key=>$value){
                $finalMsg = "Data till row number ". $rowInserted . " is added to the database";
                $X = $value['X'];
                $Y = $value['Y'];
                if (empty($X) or empty($Y)){return new Response('x or y cannot be empty at row ' . $RowInFile);}
                //Validation 2: check for pot id for the flatid and X and Y
                $resPotId = TrackDB\checkForPotId($value['Flat_id'],$X,$Y,$dbConnect);
                if(empty($resPotId[0]['Object_id'])){
                    //Validation 3: check if x any y in file are as per the max_x and max_y in database
                    $resFlatId = TrackDB\checkForFlatId($value['Flat_id'],$dbConnect);
                    if (!$resFlatId){return new Response('flat id doesnot exit in DB at row ' . $RowInFile);}
                    $TypeIdRes = TrackDB\getTypeId($resFlatId,$dbConnect);
                    if ($X <= TrackDB\checkForMax_X($TypeIdRes) and $Y <= TrackDB\checkForMax_Y($TypeIdRes)){
                        //auto-commit is set to false
                        mysqli_autocommit($conn, FALSE);
                        //if NOT PLANTED THEN ONLY POT ID IS CREATED
                        if(empty($value['Construct_id'])){
                            $pot = new TrackDB\Pot($X,$Y,$value['Flat_id'],true,$conn);
                            $pot->fillPot($conn,$pot);
                            error_log("The value of construct id from if is ".$value['Construct_id']." ---");
                        }else{//when all 4 classes are to be instantiated
                            
                            //Step 1 - create a new Construct
                            $construct = new TrackDB\Construct($value['Construct_id']);
                            $consResult = $construct->checkInsGetConstID($dbConnect,$conn);
                            
                            //Step 2 - create a new Event
                            $event = new TrackDB\Events($value['Construct_id'],$construct->Construct_id);
                            $eventResult = $event->checkInsGetEventID($dbConnect,$conn);
                            //Step 3 - create a new Pot
                            $pot = new TrackDB\Pot($X,$Y,$value['Flat_id'],false,$conn);
                            $pot->fillPot($conn,$pot);
                                
                            //Step 4 - create a new Plant
                            $plant = new TrackDB\Plant($X,$Y,$pot->potIDfromDB,$construct->Display_name,$crop,$conn);
                            $plant->checkInsGetGeneticID($dbConnect); 
                            $plant->fillPlant($conn,$plant);
                        }
                    }else{//else condition for Validation # 3
                        return new Response('X and Y at row ' . $RowInFile . ' doesnot exist in DB');}
                }else{//else condition for Validation # 2
                    return new Response('Pot for flat id,x,y at row ' . $RowInFile . ' already exists in DB');}
            $RowInFile = $RowInFile + 1;
            $rowInserted = $rowInserted + 1;
            }//closes foreach condition
            //commiting ONLY WHEN ALL DATA FOR 36 pots/plants IS OK!
            mysqli_commit($conn);
            //return new Response($plant->Genetic_id);
            return new Response($finalMsg);
        }else{//else condition for Validation # 1
            return new Response('The number of flat rows cannot exceed beyond ' . $totalPots);}
                
    }
    
    /**
    * @Route("/api/tracking/{crop}/create")
    * @Method("POST")
    */
    public function createPOSTAction(Request $request)
    {
        $request_body = $request->getContent();
        $data_decode = json_decode($request_body,true);
        //max x and max y is coming from request
        $flat_X = $data_decode['x'];
        $flat_Y = $data_decode['y'];
        $dbConnect = new TrackDB\DbConnect();
        $conn = $dbConnect->connect();
        
        $flat= new TrackDB\Flat($flat_X,$flat_Y,$conn);
        $sql = $flat->sqlObjectTable($flat);
        
        //auto-commit is set to false
        mysqli_autocommit($conn, FALSE);
        if (mysqli_query($conn,$sql)){
            $flatIDfromDB = mysqli_insert_id($conn);
            $sqlObjectTree = $flat->sqlObjectTreeTable($flatIDfromDB,$flat);
            if (mysqli_query($conn,$sqlObjectTree)){
                
                //commit ONLY IF both insert statements ARE TRUE
                mysqli_commit($conn);
                $data = array('Flat_id' => $flatIDfromDB);
                error_log( "Flat id " . $flatIDfromDB . " is created in the database." );
                return new JsonResponse($data);
            }else{
                error_log("Error updating table object_tree table: " . mysqli_error($conn));
                //ROLLBACK BOTH insert if any error occurs here
                mysqli_rollback($conn);
                return new Response("Something went wrong! Please try again later");
            }
        }else{
            $sqlMsg = "Error updating table: " . mysqli_error($conn);
            return new Response($sqlMsg);
        }
    }//for function
}//for controller class
?>
