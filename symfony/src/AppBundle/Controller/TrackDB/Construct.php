<?php

namespace AppBundle\Controller\TrackDB;


class Construct
{

    public $Display_name = 'none';
    public $Construct_id = 0;
    function __construct($disName){
        $this->Display_name = $disName;
    }
    
    function checkInsGetConstID($dbConnect,$conn){
        //check if its already there
        //if no, INSERT into the table
        //if yes don't INSERT but get the id
        $sqlSelect = "Select * from construct where Display_name='" . $this->Display_name . "'";
        //returns associative array on success and FALSE on failure
        $sqlResult = $dbConnect->dbSelect($sqlSelect);
        if ($sqlResult){
            $this->Construct_id = $sqlResult[0]['Construct_id'];
            //error_log("Construct with name ". $this->Display_name. " already exits in DB with id ".$this->Construct_id);
            return $this->Construct_id;
        }
        else{
            $sql = $this->sqlConstTbl();
            $results = mysqli_query($conn,$sql);
            //$results is either false or true for INSERT queries
            if ($results){
                $this->Construct_id = mysqli_insert_id($conn);
                error_log("A new construct is added with id ".$this->Construct_id);
                return $this->Construct_id;
            }
            else{
                error_log("Error updating table construct: " . mysqli_error($conn));
            }
            
        }
    }
    
    function sqlConstTbl(){
        $sqlStr = "INSERT INTO construct(Display_name)
                VALUES('" . $this->Display_name . "')";
        return $sqlStr;
    }
    
    
    
}
?>