<?php

namespace AppBundle\Controller\TrackDB;
require_once 'TrackObj.php';
   
// Plot sub class
//no need of description and max_x and max_y
class Pot extends TrackObj
{
    public $X = 0;
    public $Y = 0;
    public $Parent_id = 0;
    public $initial_State = 'Full';
    public $geneticDisName = 'none';
    public $Display_name = 'none';
    public $potIDfromDB = 0;
    function getClassName(){
        $className = substr(strrchr(__CLASS__, "\\"), 1);
        $this->Display_name = $className;
    }
    
    function setState(){
        //if something is true then set state = empty
        if($this->ifNotPlanted){$this->initial_State='Empty';}
    }
    
    function __construct($X,$Y,$Parent_id,$ifNotPlanted,$conn){
        $this->X = $X;
        $this->Y = $Y;
        $this->Parent_id = $Parent_id;
        $this->ifNotPlanted = $ifNotPlanted;
        $this->conn = $conn;
        $this->getClassName();
        $this->setState();
        parent::__construct($conn,
                            $this->initial_State,
                            $this->Display_name,
                            $this->geneticDisName);
    }
    
    function sqlObjectTable($potObj){
        $sqlStr = "INSERT INTO objects(Type_id,State_id,Display_name,Genetic_id)
                                VALUES('" . $potObj->Type_id . "','" . 
                                        $potObj->State_id .  "','" .
                                        $potObj->Display_name .  "','" .
                                        $potObj->Genetic_id . "')" ;
        
        return $sqlStr;
    }
    
    function sqlObjectTreeTable($potIDfromDB,$potObj){
        $sqlStr = "INSERT INTO object_tree(Object_id,X,Y,Parent_id) 
                                        VALUES('" . $potIDfromDB . "','" .
                                        $potObj->X . "','" .
                                        $potObj->Y . "','" .
                                        $potObj->Parent_id . "')" ;
        
        return $sqlStr;
    }
    
    function fillPot($conn,$potObj){
        $sql = $this->sqlObjectTable($potObj);
        if (mysqli_query($conn,$sql)){
            $this->potIDfromDB = mysqli_insert_id($conn);
            //error_log("A new pot id ".$this->potIDfromDB." for x= ".$this->X." for y= ".$this->Y);
            $sqlObjectTree = $this->sqlObjectTreeTable($this->potIDfromDB,$potObj);
            if (mysqli_query($conn,$sqlObjectTree)){
            }else{ //if object tree table is not updated
                error_log("Error updating Object tree table for pot: " . mysqli_error($conn));
            }
        }else{//if objects table is not updated
            error_log("Error updating table Objects for pot: " . mysqli_error($conn));
        }
    }
    
    
}//for Plot sub class
?>