<?php

namespace AppBundle\Controller\TrackDB;
require_once 'TrackObj.php';
   
// Flat sub class //include parent and child properties and max_x and max_y
class Flat extends TrackObj
{
    public $Max_x = 0;
    public $Max_y = 0;
    public $Description = 'none';
    public $initial_State = 'Empty';
    public $geneticDisName = 'none';
    public $parentDisname = 'none';
    public $Parent_id = 0;
    function getClassName(){
        $className = substr(strrchr(__CLASS__, "\\"), 1);
        $this->Description = $className;
    }
    
    function getParentId(){
        $sqlParentID = "SELECT Object_id from objects where Display_name='" . $this->parentDisname . "'";
        $results = mysqli_query($this->conn,$sqlParentID);
        if ($results){
                $row = mysqli_fetch_assoc($results);
                $this->Parent_id = $row['Object_id'];
        }else{
            error_log("Select query to get parent id for flat was not successful");
        }
    }
    
    function __construct($Max_x,$Max_y,$conn){
        $this->Max_x = $Max_x;
        $this->Max_y = $Max_y;
        $this->conn = $conn;
        $this->total = $Max_x * $Max_y;
        $this->getClassName();
        $this->getParentId();
        $this->Display_name = $this->Description . $this->total;
        parent::__construct($conn,
                            $this->initial_State,
                            $this->Display_name,
                            $this->geneticDisName);
    }
    
    public $X = 0;
    public $Y = 0;
    function sqlObjectTable($flatObj){
        $sqlStr = "INSERT INTO objects(Type_id,State_id,Display_name,Genetic_id)
                VALUES('" . $flatObj->Type_id . "','" . 
                        $flatObj->State_id .  "','" .
                        $flatObj->Display_name .  "','" .
                        $flatObj->Genetic_id . "')" ;
        return $sqlStr;
        
    }
    
    function sqlObjectTreeTable($flatIDfromDB,$flatObj){
        $sqlStr = "INSERT INTO object_tree(Object_id,X,Y,Parent_id)
            VALUES('" . $flatIDfromDB . "','" .
                    $flatObj->X . "','" .
                    $flatObj->Y . "','" .
                    $flatObj->Parent_id . "')" ;
        return $sqlStr;
    }
}//for Flat sub class
?>