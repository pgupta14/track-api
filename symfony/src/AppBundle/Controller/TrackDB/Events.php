<?php

namespace AppBundle\Controller\TrackDB;


class Events
{

    public $Display_name = 'none';
    public $Event_id = 0;
    public $parent_id = 0;
    public $nextDisName = 0;
    function __construct($disName,$const_id){
        $this->DisNameToCheck = $disName;
        $this->parent_id = $const_id;
    }
    
    function checkInsGetEventID($dbConnect,$conn){
        $sqlSelect = "Select * from events where Display_name LIKE'" . $this->DisNameToCheck . "%' ORDER BY Event_id DESC";
        //returns associative array on success and FALSE on failure
        $sqlResult = $dbConnect->dbSelect($sqlSelect);
        if ($sqlResult){
            //gets the position of -
            $separator = strpos($sqlResult[0]['Display_name'],'-') + 1;
            //extract the number after '-'
            $theEventNumInDb = substr($sqlResult[0]['Display_name'], $separator);
            $nextEventNum = $theEventNumInDb + 1;
            $this->Display_name = $this->DisNameToCheck . '-'. $nextEventNum;
            $this->fillEvent($conn);
        }
        else{
            error_log("hEY!i AM HERE and creating a new event!");
            $this->Display_name = $this->DisNameToCheck . '-' . 1;
            $this->fillEvent($conn);
        }
    }
    
    function sqlEventTbl(){
        $sqlStr = "INSERT INTO events(Display_name, Construct_id)
                VALUES('" . $this->Display_name . "','" . $this->parent_id . "')" ;
        return $sqlStr;
    }
    
    function fillEvent($conn){
        $sql = $this->sqlEventTbl();
        $results = mysqli_query($conn,$sql);
        //$results is either false or true for INSERT queries
        if ($results){//error_log("A new event is added with name ".$this->Display_name);
                     }
        else{error_log("Error updating table Event: " . mysqli_error($conn));}
    }
}
?>