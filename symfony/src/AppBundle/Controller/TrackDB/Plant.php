<?php

namespace AppBundle\Controller\TrackDB;
require_once 'TrackObj.php';
   
// Plant sub class
class Plant extends TrackObj
{
    public $X = 0;
    public $Y = 0;
    public $Parent_id = 0;
    public $speciesNm = 'none';
    //public $speciesNm = 'Setaria Viridis';
    public $Species_id = 0;
    public $initial_State = 'Planted';
    public $geneticDisName = 'none';
    public $Display_name = 'none';
    public $Company_id = 1;//this is for Benson Hill bio in DB
    //public $plantGenetic_id = 0;
    public $plantIDfromDB = 0;
    function getClassName(){
        $className = substr(strrchr(__CLASS__, "\\"), 1);
        $this->Display_name = $className;
    }
    
    function __construct($X,$Y,$Parent_id,$geneticDisName,$speciesNm,$conn){
        $this->X = $X;
        $this->Y = $Y;
        $this->Parent_id = $Parent_id;
        $this->geneticDisName = $geneticDisName;
        $this->speciesNm = $speciesNm;
        $this->conn = $conn;
        $this->getClassName();
        //Call the parent class's constructor
        parent::__construct($conn,
                            $this->initial_State,
                            $this->Display_name,
                            $this->geneticDisName);
        $this->getSpeciesId();
    }
    
    function getSpeciesId(){
        $sqlSpecies = "Select Species_id from species where Sci_name='" . $this->speciesNm . "'";
        //returns associative array on success and FALSE on failure
        $results = mysqli_query($this->conn,$sqlSpecies);
        
        if ($results){
                $row = mysqli_fetch_assoc($results);
                $this->Species_id = $row['Species_id'];
        }else{
            //TODO: log the error in the error file
            error_log("Error running select query on species table: " . mysqli_error($this->conn));
        }
    }
    
    function checkInsGetGeneticID($dbConnect){
        //check if its already there
        //if no, INSERT into the table
        //if yes don't INSERT but get the id
        $sqlSelect = "Select * from genetics where Display_name='" . $this->geneticDisName . "' AND Species_id=" . $this->Species_id;
        //returns associative array on success and FALSE on failure
        $results = $dbConnect->dbSelect($sqlSelect);
        if ($results){
                $this->Genetic_id = $results[0]['Genetic_id'];
        }
        else{
            $sql = $this->sqlGeneticsTbl();
            $results = mysqli_query($this->conn,$sql);
            //$results is either false or true for INSERT queries
            if ($results){
                //$sqlMsg = "Construct table is updated";
                $this->Genetic_id = mysqli_insert_id($this->conn); 
                //return $this->Genetic_id;
                }
                else{
                error_log("Error updating table genetics: " . mysqli_error($this->conn));
                //enter to the log file
                }
        }
        
    }
    
    function sqlGeneticsTbl(){
        $sqlStr = "INSERT INTO genetics(Species_id,Company_id,Display_name)
                VALUES('".  $this->Species_id . "','" .
                            $this->Company_id . "','" .
                            $this->geneticDisName . "')" ;
        return $sqlStr;
    }
    
    function sqlObjectTable($plantObj){
        $sqlStr = "INSERT INTO objects(Type_id,State_id,Display_name,Genetic_id)
                                VALUES('" . $plantObj->Type_id . "','" . 
                                        $plantObj->State_id .  "','" .
                                        $plantObj->Display_name .  "','" .
                                        $plantObj->Genetic_id . "')" ;
        
        return $sqlStr;
    }
    
    function sqlObjectTreeTable($plantIDfromDB,$plantObj){
        $sqlStr = "INSERT INTO object_tree(Object_id,X,Y,Parent_id) 
                                        VALUES('" . $plantIDfromDB . "','" .
                                        $plantObj->X . "','" .
                                        $plantObj->Y . "','" .
                                        $plantObj->Parent_id . "')" ;
        
        return $sqlStr;
    }
    
    function fillPlant($conn,$plantObj){
        $sql = $this->sqlObjectTable($plantObj);
        if (mysqli_query($conn,$sql)){
            $this->plantIDfromDB = mysqli_insert_id($conn);
            //error_log("A new plant is added with id ".$this->plantIDfromDB . " for x= ". $this->X . " for y= " . $this->Y);
            $sqlObjectTree = $this->sqlObjectTreeTable($this->plantIDfromDB,$plantObj);
            if (mysqli_query($conn,$sqlObjectTree)){
            }else{ //if object tree table is not updated
                error_log("Error updating table Object tree table for plant: " . mysqli_error($conn));
            }
        }else{//if objects table is not updated
            error_log("Error updating table Objects for plant: " . mysqli_error($conn));
        }
        //return $sqlMsg;
    }
}//for Plant sub class
?>