<?php

namespace AppBundle\Controller\TrackDB;

//abstract classes are used to enforce a protocol. 
//A protocol is a set of operations which all implementing objects must support
//Members declared protected can be accessed only within the class itself and by //inherited and parent classes. and not by objects created from class itself
//http://stackoverflow.com/questions/6307107/php-cannot-access-protected-property-error
abstract class TrackObj
{
    protected $Type_id = 0;
    protected $State_id = 0;
    protected $Genetic_id = 0;
    function __construct($conn,$iniState,$disName,$GeneticDisName){
        $this->conn = $conn;
        $this->iniState = $iniState;
        $this->disName = $disName;
        $this->GeneticDisNameParent = $GeneticDisName;
        $this->getTypeId();
        $this->getStateId();
        $this->getGeneticId();
    }
    
    protected function getTypeId(){
        $sqlTypeID = "SELECT Type_id from object_type where Display_name='" . $this->disName . "'";
        $results = mysqli_query($this->conn,$sqlTypeID);
        
        
        if ($results){
                $row = mysqli_fetch_assoc($results);
                $this->Type_id = $row['Type_id'];
        }else{
            //TODO: log the error in the error file
        }
    }
    
    protected function getStateId(){
        $sqlStateID = "SELECT State_id from states where Display_name='" . $this->iniState . "'";
        $results = mysqli_query($this->conn,$sqlStateID);
        
        if ($results){
                $row = mysqli_fetch_assoc($results);
                $this->State_id = $row['State_id'];
        }else{
            //TODO: log the error in the error file
        }
    }
    
    protected function getGeneticId(){
        $sqlGeneticID = "SELECT Genetic_id from genetics where Display_name='" . $this->GeneticDisNameParent . "'";
        $results = mysqli_query($this->conn,$sqlGeneticID);
        
        if ($results){
                $row = mysqli_fetch_assoc($results);
                $this->Genetic_id = $row['Genetic_id'];
        }else{//return 'Not Found';
            //TODO: log the error in the error file
        }
    }

};
?>