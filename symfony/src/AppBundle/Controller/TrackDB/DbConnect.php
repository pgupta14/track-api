<?php
namespace AppBundle\Controller\TrackDB;
//$this->member for non-static members
//self::$member for static members
//http://board.phpbuilder.com/showthread.php?10354489-RESOLVED-php-class-quot-this-quot-or-quot-self-quot-keyword

class DbConnect
{
    //Define connection as a static variable, to avoid connecting more than once
    protected static $conn;
    /**
    * Connect to the database
    * 
    * @return bool false on failure / mysqli MySQLi object instance on success
    */
    public function connect(){
        //Try and connect to the database,if a conn has not been established yet
        if(!isset(self::$conn)) {
            //Load configuration as an array.
            $config = parse_ini_file('config.ini'); 
            //self::$conn = mysqli_connect("52.91.159.120:3306",$config['username'],$config['password'],$config['dbname']);
            self::$conn =mysqli_connect("localhost:3306",$config['username'],$config['password'],$config['dbname']);
            return self::$conn;
        }
        //If connection was not successful, handle the error
        if (self::$conn === false) {
            error_log("Error connecting with database: " .mysqli_connect_error());
        }   
        
    }
    
    public function DbQuery($query){
        //Returns FALSE on failure. For successful SELECT queries mysqli_query() will return a mysqli_result //object. For other successful queries mysqli_query() will return //TRUE.
        $result = mysqli_query(self::$conn,$query);
        return $result;
    }
    
    public function DbError(){
        $conn = $this->connect();
        return mysqli_error($conn);
    }
    
    public function dbSelect($query){
        $rows = array();
        $result = $this->DbQuery($query);

        //If query failed, return `false`
        if($result === false){
            //return mysqli_error(self::$conn);
            error_log(mysqli_error(self::$conn));
            return false;
        }

        //If query was successful, retrieve all the rows into an array
        while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }

}//for Db class
?>