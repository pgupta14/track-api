<?php

namespace AppBundle\Controller\TrackDB;

//$r = mysql_query("SELECT 1 FROM user WHERE state = 'Alabama' LIMIT 1");
function checkForFlatId($flatId,$dbConnect){
    $flatIdFile = $flatId;
    $sqlFlatId = "Select * from objects where Object_id=" . $flatIdFile;
    //returns an associative array
    $flatObjRes = $dbConnect->dbSelect($sqlFlatId);
    //returns false on failure
    return $flatObjRes;
}

function checkForPotId($flatId,$X,$Y,$dbConnect){
    $flatIdFile = $flatId;
    $sqlPotId = "Select Object_id from object_tree where Parent_id=" . $flatIdFile . " AND X=" . $X . " AND Y=" . $Y ;
    //returns an associative array
    $potObjRes = $dbConnect->dbSelect($sqlPotId);
    //returns false on failure
    return $potObjRes;
}

function getTypeId($resFlatId,$dbConnect){
    $flatTypeID = $resFlatId[0]['Type_id'];
    //get the type id of the flat from DB
    $sqlFlatTypeID = "Select * from object_type where Type_id=" . $flatTypeID;
    $TypeIdRes = $dbConnect->dbSelect($sqlFlatTypeID);
    return $TypeIdRes;
}

function checkForTotalChild($TypeIdRes){
    $totalPots = $TypeIdRes[0]['Max_x'] * $TypeIdRes[0]['Max_y'];
    return $totalPots;
}

function checkForMax_X($TypeIdRes){
    return $TypeIdRes[0]['Max_x'];
}

function checkForMax_Y($TypeIdRes){
   return $TypeIdRes[0]['Max_y'];
}



?>