-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2016 at 10:31 PM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `track_test`

-- --------------------------------------------------------
USE `Track_test`;
--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `Company_id` int(11) NOT NULL AUTO_INCREMENT,
  `Company_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`Company_id`, `Company_name`) VALUES
(1, 'Benson Hill Biosystems');

-- --------------------------------------------------------

--
-- Table structure for table `construct`
--

DROP TABLE IF EXISTS `construct`;
CREATE TABLE IF NOT EXISTS `construct` (
  `Construct_id` int(11) NOT NULL AUTO_INCREMENT,
  `Display_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Construct_id`),
  UNIQUE KEY `Display_name` (`Display_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `construct`
--

INSERT INTO `construct` (`Construct_id`, `Display_name`) VALUES
(1, '1234567');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `Event_id` int(11) NOT NULL AUTO_INCREMENT,
  `Display_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `Copy_number` double DEFAULT NULL,
  `Construct_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Event_id`),
  UNIQUE KEY `Display_name` (`Display_name`),
  KEY `Construct_id` (`Construct_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `genetics`
--

DROP TABLE IF EXISTS `genetics`;
CREATE TABLE IF NOT EXISTS `genetics` (
  `Genetic_id` int(11) NOT NULL AUTO_INCREMENT,
  `Species_id` int(11) NOT NULL,
  `Company_id` int(11) NOT NULL,
  `Display_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'none value',
  `Male_parent` int(11) DEFAULT NULL COMMENT 'can be int or null',
  `Female_parent` int(11) DEFAULT NULL COMMENT 'can be int or null',
  `Gender` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'can have M,F,V,unknown',
  `Breeding_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'INbred, Hybrid, Segpop',
  PRIMARY KEY (`Genetic_id`),
  UNIQUE KEY `Display_name` (`Display_name`),
  KEY `Species_id` (`Species_id`),
  KEY `Company_id` (`Company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `genetics`
--

INSERT INTO `genetics` (`Genetic_id`, `Species_id`, `Company_id`, `Display_name`, `Male_parent`, `Female_parent`, `Gender`, `Breeding_type`) VALUES
(1, 2, 1, 'none', NULL, NULL, 'none', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `objects`
--

DROP TABLE IF EXISTS `objects`;
CREATE TABLE IF NOT EXISTS `objects` (
  `Object_id` int(11) NOT NULL AUTO_INCREMENT,
  `Type_id` int(11) NOT NULL,
  `State_id` int(11) NOT NULL,
  `Display_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `Created_Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Modified_Date` timestamp NULL DEFAULT NULL,
  `Genetic_id` int(11) NOT NULL,
  PRIMARY KEY (`Object_id`),
  KEY `Type_id` (`Type_id`),
  KEY `State_id` (`State_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1008 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `objects`
--

INSERT INTO `objects` (`Object_id`, `Type_id`, `State_id`, `Display_name`, `Created_Date`, `Modified_Date`, `Genetic_id`) VALUES
(1, 4, 8, 'none', '2016-05-18 13:42:20', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `object_tree`
--

DROP TABLE IF EXISTS `object_tree`;
CREATE TABLE IF NOT EXISTS `object_tree` (
  `Object_id` int(11) NOT NULL,
  `X` tinyint(4) NOT NULL,
  `Y` tinyint(4) NOT NULL,
  `Parent_id` int(11) DEFAULT NULL,
  UNIQUE KEY `Object_id` (`Object_id`),
  KEY `Parent_id` (`Parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `object_type`
--

DROP TABLE IF EXISTS `object_type`;
CREATE TABLE IF NOT EXISTS `object_type` (
  `Type_id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Display_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `Max_x` smallint(6) NOT NULL,
  `Max_y` smallint(6) NOT NULL,
  PRIMARY KEY (`Type_id`),
  UNIQUE KEY `Display_name` (`Display_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `object_type`
--

INSERT INTO `object_type` (`Type_id`, `Description`, `Display_name`, `Max_x`, `Max_y`) VALUES
(1, 'Flat', 'Flat36', 4, 9),
(2, 'Pots for flat', 'Pot', 1, 1),
(3, 'Plant for flat', 'Plant', 1, 1),
(4, 'none', 'none', 0, 0),
(6, 'Flat', 'Flat18', 3, 6);

-- --------------------------------------------------------

--
-- Table structure for table `pipeline`
--

DROP TABLE IF EXISTS `pipeline`;
CREATE TABLE IF NOT EXISTS `pipeline` (
  `Pipeline_id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Pipeline_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `species`
--

DROP TABLE IF EXISTS `species`;
CREATE TABLE IF NOT EXISTS `species` (
  `Species_id` int(11) NOT NULL AUTO_INCREMENT,
  `Sci_name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `Taxonomy` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Species_id`),
  UNIQUE KEY `Sci_name` (`Sci_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `species`
--

INSERT INTO `species` (`Species_id`, `Sci_name`, `Taxonomy`) VALUES
(1, 'Setaria Viridis', NULL),
(2, 'none', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE IF NOT EXISTS `states` (
  `State_id` int(11) NOT NULL AUTO_INCREMENT,
  `Display_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`State_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`State_id`, `Display_name`) VALUES
(1, 'Empty'),
(2, 'Full'),
(3, 'Planted'),
(4, 'Sampled'),
(5, 'Discard'),
(6, 'Dead'),
(7, 'Active'),
(8, 'none');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `object_tree`
--
ALTER TABLE `object_tree`
  ADD CONSTRAINT `object_tree_ibfk_1` FOREIGN KEY (`Parent_id`) REFERENCES `objects` (`Object_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
